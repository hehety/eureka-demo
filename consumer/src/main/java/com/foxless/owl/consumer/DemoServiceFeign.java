package com.foxless.owl.consumer;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service")
@RibbonClient(name = "service")
public interface DemoServiceFeign {

    @GetMapping("/hello")
    @LoadBalanced
    String hello(@RequestParam("from") String from);
}
